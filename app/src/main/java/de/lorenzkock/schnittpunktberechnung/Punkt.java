package de.lorenzkock.schnittpunktberechnung;

/**
 * Created by lkock on 12.04.2017.
 */

public class Punkt {

    private double x, y;

    public Punkt(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
