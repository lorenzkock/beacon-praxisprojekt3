package de.lorenzkock.schnittpunktberechnung;

/**
 * Created by lkock on 12.04.2017.
 */

public class Schnittpunktberechnung {

    public static void main(String[] args) {
        Kreis kreis1 = new Kreis(new Punkt(3,0),5);
        Kreis kreis2 = new Kreis(new Punkt(7, 6), 3);
        Kreis kreis3 = new Kreis(new Punkt(11,3),4);

        Grade g1 = kreis1.schneideMit(kreis2);
        Grade g2 = kreis2.schneideMit(kreis3);

        Punkt s = g1.schneideMit(g2);

    }

}
