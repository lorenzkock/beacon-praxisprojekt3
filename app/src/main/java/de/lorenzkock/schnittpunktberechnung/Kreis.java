package de.lorenzkock.schnittpunktberechnung;

/**
 * Created by lkock on 12.04.2017.
 */

public class Kreis {

    private Punkt m;
    private double r;

    public Kreis(Punkt m, double r) {
        this.m = m;
        this.r = r;
    }

    public Grade schneideMit(Kreis kreis) {

        // 2. Binomische Formel auflösen in Kreis 1

        double k1FaktorX = -2 * this.m.getX();
        double k1Koef1 = Math.pow(this.m.getX(), 2);

        double k1FaktorY = -2 * this.m.getY();
        double k1Koef2 = Math.pow(this.m.getY(), 2);

        double k1rQ = Math.pow(this.r, 2);
        k1rQ -= (k1Koef1 + k1Koef2);

        // 2. Binomische Formel auflösen in Kreis 2

        double k2FaktorX = -2 * kreis.m.getX();
        double k2Koef1 =
                Math.pow(kreis.m.getX(), 2);

        double k2FaktorY = -2 * kreis.m.getY();
        double k2Koef2 =
                Math.pow(kreis.m.getY(), 2);

        double k2rQ = Math.pow(kreis.r, 2);
        k2rQ -= (k2Koef1 + k2Koef2);

        // Gradengleichung durch Subtraktion der Kreise
        double gradeFaktorX = k1FaktorX - k2FaktorX;
        double gradeFaktorY = k1FaktorY - k2FaktorY;

        double gradeErgebnis = k1rQ - k2rQ;

        gradeFaktorX *= -1;
        gradeFaktorX /= gradeFaktorY;
        gradeErgebnis /= gradeFaktorY;

        return new Grade(gradeFaktorX,
                gradeErgebnis);
    }

}
