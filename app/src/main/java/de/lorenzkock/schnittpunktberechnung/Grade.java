package de.lorenzkock.schnittpunktberechnung;

/**
 * Created by lkock on 12.04.2017.
 */

public class Grade {

    private double a, b;

    public Grade(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getY(double x) {
        return this.a * x + this.b;
    }

    public Punkt schneideMit(Grade g) {
        double newA = this.a - g.a;
        double newB = g.b - this.b;
        double xSchnitt = newB / newA;

        double y = this.getY(xSchnitt);

        return new Punkt(xSchnitt, y);

    }
}
