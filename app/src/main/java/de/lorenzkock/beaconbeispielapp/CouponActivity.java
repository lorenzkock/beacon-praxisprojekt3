package de.lorenzkock.beaconbeispielapp;

import android.app.Activity;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.android.volley.Response;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Distance;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CouponActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient     apiClient;
    private MessageListener     mMessageListener;
    private BCMSConnector       bcmsConnector = new BCMSConnector();
    public ArrayList<Campaign>  campaigns = new ArrayList<Campaign>();

    // Aufruf beim Starten der Ansicht
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);

        // Verbindung zum Google API Client herstellen
        this.apiClient = new GoogleApiClient.Builder(this)
                .addApi(Nearby.MESSAGES_API)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .build();

        // Message Listener implementieren: Was geschieht, wenn eine neue Nachricht gefunden wurde.
        this.mMessageListener = new MessageListener() {
            @Override
            public void onFound(Message message) {
                String messageAsString = new String(message.getContent());
                if(message.getType().equals("json")) {
                    foundBeacon(message);
                }
            }
        };
    }

    // Aktuallisieren der ListView aus den geladenen Kampagnendaten
    private void populateListView() {

        // Nur aktuallisieren, wenn auch Kampagnen vorhanden sind
        if(campaigns.size() > 0) {

            // ListView einblenden und Text ausblenden
            ListView list = (ListView)findViewById(R.id.coupon_list);
            list.setVisibility(View.VISIBLE);
            findViewById(R.id.akt_text_view).setVisibility(View.INVISIBLE);

            // Liste mit Kampagnen erstellen, die von der ListView verstanden wird
            List<Map<String, String>> values = new ArrayList<Map<String, String>>();
            Map<String, String> map = null;

            for (int i=0;i<campaigns.size();i++) {

                map = new HashMap<String,String>();

                map.put("coupon_cell_title", campaigns.get(i).getTitle());

                map.put("coupon_cell_description", campaigns.get(i).getDescription());

                values.add(map);

            }
            String[] from = new String[]{"coupon_cell_title","coupon_cell_description"};
            int[] to = new int[]{R.id.coupon_cell_title,R.id.coupon_cell_description};

            SimpleAdapter adapter = new SimpleAdapter(CouponActivity.this, values, R.layout.coupon_cell, from, to);
            list.setAdapter(adapter);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Bei erfolgreicher Verbindung wird umgehend der Scan gestartet
        SubscribeOptions options = new SubscribeOptions.Builder()
                .setStrategy(Strategy.BLE_ONLY)
                .build();
        Nearby.Messages.subscribe(apiClient, mMessageListener, options);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    // Wenn ein Beacon gefunden wurde, sollen weitere Informationen zu den zugehörigen Kampagnen abgefragt werden
    private void foundBeacon(Message message) {

        String messageAsString = new String(message.getContent());
        String cuid = "";
        try {
            JSONObject beaconJSONObject = new JSONObject(messageAsString);
            if(beaconJSONObject.has("cuid")) {
                cuid = beaconJSONObject.getString("cuid");
                if(cuid.length() > 0) {
                    // Überprüfen, ob Kampagne bereits vorhanden
                    for(Campaign cTmp : campaigns) {
                        if(cTmp.getId().equals(cuid)) {
                            return;
                        }
                    }
                    this.getCampaign(cuid);
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Lade die Kampagnendaten
    public void getCampaign(String cuid) {

        String url = "https://audience-engagement.de/bcms/campaigns/search/findOneByCuidAndTenant?cuid="+cuid+"&tenant=https://audience-engagement.de/bcms/tenants/4&projection=campaignData";
        this.bcmsConnector.getJSONObject(this, url, "lkock@vater-gruppe.de", "*********",  new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Campaign c = new Campaign(response);
                for(Campaign cTmp : campaigns) {
                    if(c.hashCode() == cTmp.hashCode() ) {
                        if(c.equals(cTmp)) {
                            return;
                        }
                    }
                }
                campaigns.add(c);
                populateListView();

            }
        });
    }
}
