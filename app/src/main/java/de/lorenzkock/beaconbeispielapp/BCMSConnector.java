package de.lorenzkock.beaconbeispielapp;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.Authenticator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lkock on 11.04.2017.
 */

// Der BCMS Connector stellt die Verbindung zum BCMS her und stellt die Anfragen.
public class BCMSConnector {

    public void getJSONObject(Context context, String url, final String username, final String password, Response.Listener<JSONObject> responseListener) {

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null, responseListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                // Error handling
                Log.d("MESSAGE","Something went wrong!");
                error.printStackTrace();

            }
        }){
            // Authorisierung mittels Username & Passwort
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                String creds = String.format("%s:%s",username,password);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }
        };

        // Add the request to the queue
        Volley.newRequestQueue(context).add(jsonRequest);
    }



}
