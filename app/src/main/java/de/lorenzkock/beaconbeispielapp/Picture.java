package de.lorenzkock.beaconbeispielapp;

/**
 * Created by lkock on 05.04.2017.
 */

class Picture {

    private String title;
    private String description;
    private int pictureResourceId;
    private String place;
    private String year;
    private String painter;
    private String source;



    public Picture(String title, String description, int pictureResourceId, String place, String year, String painter, String source) {
        this.title = title;
        this.description = description;
        this.pictureResourceId = pictureResourceId;
        this.place = place;
        this.year = year;
        this.painter = painter;
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPictureResourceId() {
        return pictureResourceId;
    }

    public void setPictureResourceId(int pictureResourceId) {
        this.pictureResourceId = pictureResourceId;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPainter() {
        return painter;
    }

    public void setPainter(String painter) {
        this.painter = painter;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", pictureResourceId=" + pictureResourceId +
                ", place='" + place + '\'' +
                ", year='" + year + '\'' +
                ", painter='" + painter + '\'' +
                '}';
    }
}
