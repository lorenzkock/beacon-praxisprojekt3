package de.lorenzkock.beaconbeispielapp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lkock on 11.04.2017.
 */

public class Campaign {

    // Class vars
    private String id;
    private String title;
    private String description;

    public Campaign(String id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Campaign(JSONObject obj) {
        try {
            this.id = obj.getString("cuid");
            this.title = obj.getString("name");
            this.description = obj.getString("description");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Campaign campaign = (Campaign) o;

        if (id != null ? !id.equals(campaign.id) : campaign.id != null) return false;
        if (title != null ? !title.equals(campaign.title) : campaign.title != null) return false;
        return description != null ? description.equals(campaign.description) : campaign.description == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
