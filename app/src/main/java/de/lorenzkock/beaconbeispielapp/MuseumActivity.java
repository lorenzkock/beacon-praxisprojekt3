package de.lorenzkock.beaconbeispielapp;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeOptions;

import org.json.JSONArray;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

public class MuseumActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private MessageListener     mMessageListener;
    private GoogleApiClient     apiClient;
    private String              actualPicture;

    // Aufruf beim Starten der Ansicht
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_museum);
        super.onCreate(savedInstanceState);

        // Festlegen, was bei einer gefundenen Nachricht geschehen soll
        mMessageListener = new MessageListener() {
            @Override
            public void onFound(Message message) {
                String messageAsString = new String(message.getContent());
                if(message.getType().equals("museum")) {
                    // Überschreiben des aktuellen Bildes
                    actualPicture = messageAsString;
                    changePic();
                }
            }
        };

        // Verbindung zum Google API Client herstellen
        this.apiClient = new GoogleApiClient.Builder(this)
                .addApi(Nearby.MESSAGES_API)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .build();

    }

    // Bei erfolgreicher Verbindung den Scan starten
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        SubscribeOptions options = new SubscribeOptions.Builder()
                .setStrategy(Strategy.BLE_ONLY)
                .build();
        Nearby.Messages.subscribe(this.apiClient, mMessageListener, options);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    // Methode zum Ändern des dargestellten Bildes
    public void changePic() {
        Picture pic = readPicture(actualPicture);

        ((ImageView)findViewById(R.id.imageView)).setImageResource(pic.getPictureResourceId());

        ((TextView)findViewById(R.id.imageTitle)).setText(pic.getTitle());
        ((TextView)findViewById(R.id.imageDescription)).setText(pic.getDescription());
        ((TextView)findViewById(R.id.painterVar)).setText(pic.getPainter());
        ((TextView)findViewById(R.id.paintedVar)).setText(pic.getYear());
        ((TextView)findViewById(R.id.placeVar)).setText(pic.getPlace());
        ((TextView)findViewById(R.id.source)).setText("Quelle: " +pic.getSource());

        ((TextView)findViewById(R.id.painter)).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.painted)).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.place)).setVisibility(View.VISIBLE);

    }

    // Resource-ID mit Hilfe des Bildnamens finden
    public int getResourceId(String pVariableName, String pResourcename, String pPackageName)
    {
        try {
            return getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    // Lesen der Bild-Informationen aus der JSON-Datei
    private Picture readPicture(String pictureId) {

        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.pictures);

        Scanner scanner = new Scanner(is);

        StringBuilder str = new StringBuilder();
        while(scanner.hasNextLine()) {
            str.append(scanner.nextLine());
        }

        try {
            JSONArray array = new JSONArray(str.toString());
            JSONObject obj = new JSONObject();

            for(int i=0;i< array.length(); i++) {
                obj = array.getJSONObject(i);

                if(obj.getString("id").equals(pictureId)) {
                    // Neues Bild-Objekt erstellen und zurückgeben
                    Picture pic = new Picture(obj.getString("title"),
                                        obj.getString("description"),
                                        getResourceId(obj.getString("res"),"drawable",getPackageName()),
                                        obj.getString("place"),
                                        obj.getString("year"),
                                        obj.getString("painter"),
                                        obj.getString("source"));

                    return pic;
                }
            }

        } catch (Exception e) {

        }

        return null;

    }
}
